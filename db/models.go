package db

import (
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
)

type ThaiTest struct {
	Name string `gorm:"type:text;primaryKey"`
	Age  int    `gorm:"type:int"`
	Role string `gorm:"type:text"`
}

func (*ThaiTest) TableName() string {
	return "thaitest"
}

// Model definition.
type Model interface {
	schema.Tabler
}

// Function definition.
type Function interface {
	MigrateFunction(db *gorm.DB) error
}

// Trigger definition.
type Trigger interface {
	MigrateTrigger(db *gorm.DB) error
}

func GetModelList() []Model {
	return []Model{
		&ThaiTest{},
	}
}
