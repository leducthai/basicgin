package db

import (
	"fmt"

	"github.com/gin-gonic/gin"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"

	"gitlab.com/leducthai/basicgin/db/logger"
)

type DataManager struct {
	db *gorm.DB
}

type PGConfig struct {
	Address  string
	Port     int
	UserName string
	Password string
	Database string
}

type options struct {
	schema            string
	autoMigrateTables bool
}

type Option func(*options)

func parseOptions(opts []Option) options {
	var o options
	for _, opt := range opts {
		opt(&o)
	}
	return o
}

// WithPostgreSQLSchema with given schema name for postgreSQL.
func WithPostgreSQLSchema(schema string) Option {
	return func(o *options) {
		o.schema = schema
	}
}

// AutoMigrateTables make the system migrate tables automatically.
func AutoMigrateTables() Option {
	return func(o *options) {
		o.autoMigrateTables = true
	}
}

func newDataManager(cfg PGConfig, o options) (*DataManager, error) {
	db, err := NewDB(cfg, DBOptions{
		Schema: o.schema,
		Logger: logger.NewLogger(),
	})
	if err != nil {
		return nil, err
	}
	return &DataManager{db: db}, nil

}

func New(
	ctx *gin.Context,
	pgConfig PGConfig,
	opts ...Option,
) (DataManager, error) {

	o := parseOptions(opts)

	dm, err := newDataManager(pgConfig, o)
	if err != nil {
		return DataManager{}, err
	}

	if o.autoMigrateTables {
		if err := dm.maybeMigrate(); err != nil {
			return DataManager{}, err
		}
	}

	return *dm, nil
}

func (dm *DataManager) maybeMigrate() error {
	ms := GetModelList()

	if err := maybeMigrateTables(dm.db, ms...); err != nil {
		return err
	}

	if err := maybeMigrateFunctions(dm.db, ms...); err != nil {
		return err
	}

	return maybeMigrateTriggers(dm.db, ms...)
}

// DBOptions are option settings for gorm database.
type DBOptions struct {
	Schema string
	Logger logger.Interface
}

func NewDB(cfg PGConfig, opts DBOptions) (*gorm.DB, error) {
	connectionString := fmt.Sprintf(
		"host=%s port=%d user=%s dbname=%s password=%s sslmode=disable",
		cfg.Address,
		cfg.Port,
		cfg.UserName,
		cfg.Database,
		cfg.Password,
	)
	if opts.Schema != "" {
		connectionString += fmt.Sprintf(" search_path=%s", opts.Schema)
	}
	db, err := gorm.Open(postgres.New(
		postgres.Config{
			DSN: connectionString,
		}), &gorm.Config{
		Logger: opts.Logger,
	})
	if err != nil {
		return nil, fmt.Errorf("failed to connect to postgreSQL database: %v", err)
	}
	return db, nil
}

// maybeMigrateTables attempts to create tables automatically if implement
// models.Model interface.
func maybeMigrateTables(db *gorm.DB, ms ...Model) error {
	dst := []any{}
	for _, m := range ms {
		dst = append(dst, m)
	}
	return db.AutoMigrate(dst...)
}

// maybeMigrateFunctions attempts to create functions automatically if implement
// models.Function interface.
func maybeMigrateFunctions(db *gorm.DB, ms ...Model) error {
	for _, m := range ms {
		f, ok := m.(Function)
		if !ok {
			continue
		}
		if err := f.MigrateFunction(db); err != nil {
			return err
		}
	}
	return nil
}

// maybeMigrateTriggers attempts to create triggers automatically if implement
// models.Trigger interface.
func maybeMigrateTriggers(db *gorm.DB, ms ...Model) error {
	for _, m := range ms {
		t, ok := m.(Trigger)
		if !ok {
			continue
		}
		if err := t.MigrateTrigger(db); err != nil {
			return err
		}
	}
	return nil
}

// Close implements DataManager interface.
func (dm *DataManager) Close() error {
	db, err := dm.db.DB()
	if err != nil {
		return err
	}
	return db.Close()
}
