package logger

import (
	"context"
	"fmt"
	"time"

	"go.uber.org/zap"
	"gorm.io/gorm/logger"
	"gorm.io/gorm/utils"
)

type key int

const (
	keyRequestID key = iota + 1
	keyLogger
	keyUserID
)

const (
	Silent = logger.Silent
	Error  = logger.Error
	Warn   = logger.Warn
	Info   = logger.Info
)

type Interface = logger.Interface

// Logger definition.
type Logger struct {
	logLevel logger.LogLevel
}

// NewLogger returns a new logger with log level Info.
func NewLogger() Interface {
	return &Logger{logLevel: logger.Info}
}

// LogMode resets log level.
func (l *Logger) LogMode(level logger.LogLevel) Interface {
	newLogger := *l
	newLogger.logLevel = level
	return &newLogger
}

// Info logs formatted information message with corresponding message data.
func (l *Logger) Info(ctx context.Context, msg string, data ...any) {
	if l.logLevel >= logger.Info {
		var log *zap.Logger
		if v, ok := ctx.Value(keyLogger).(*zap.Logger); ok {
			log = v
		} else {
			log = zap.L()
		}

		log.Info(fmt.Sprintf(msg, data...),
			zap.String("caller", utils.FileWithLineNum()))
	}
}

// Error logs formatted error message with corresponding message data.
func (l *Logger) Error(ctx context.Context, msg string, data ...any) {
	if l.logLevel >= logger.Error {
		var log *zap.Logger
		if v, ok := ctx.Value(keyLogger).(*zap.Logger); ok {
			log = v
		} else {
			log = zap.L()
		}

		log.Error(fmt.Sprintf(msg, data...),
			zap.String("caller", utils.FileWithLineNum()))
	}
}

// Warn logs formatted Warning message with corresponding message data.
func (l *Logger) Warn(ctx context.Context, msg string, data ...any) {
	if l.logLevel >= logger.Warn {
		var log *zap.Logger
		if v, ok := ctx.Value(keyLogger).(*zap.Logger); ok {
			log = v
		} else {
			log = zap.L()
		}

		log.Warn(fmt.Sprintf(msg, data...),
			zap.String("caller", utils.FileWithLineNum()))
	}
}

// Trace writes the tracing logs on console.
// Customize the logging info using the associated context logger.
func (l Logger) Trace(ctx context.Context, begin time.Time, fc func() (sql string, rowsAffected int64), err error) {
	elapsed := time.Since(begin)
	sql, rows := fc()
	fields := []zap.Field{
		zap.String("caller", utils.FileWithLineNum()),
		zap.Duration("elapsed_time", elapsed),
		zap.String("sql", sql),
	}
	if rows != -1 {
		fields = append(fields, zap.Int64("rows_affected", rows))
	}

	var log *zap.Logger
	if v, ok := ctx.Value(keyLogger).(*zap.Logger); ok {
		log = v
	} else {
		log = zap.L()
	}

	if err != nil && l.logLevel >= logger.Error {
		fields = append(fields, zap.Error(err))
		log.Error("tracing SQL..", fields...)
	} else {
		log.Info("tracing SQL..", fields...)
	}
}
