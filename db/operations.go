package db

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/leducthai/basicgin/server/request"
)

func (db DataManager) PostUser(cxt *gin.Context, req request.PostRequest) (request.Reply, error) {
	user := ThaiTest{
		Name: *req.Name,
		Age:  *req.Age,
		Role: *req.Role,
	}
	rt := db.db.Create(&user)

	return request.Reply{RowsAffecteds: rt.RowsAffected}, rt.Error
}
