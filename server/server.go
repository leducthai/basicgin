package server

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/go-openapi/swag"
	"github.com/jessevdk/go-flags"
	"go.uber.org/zap"
	"gopkg.in/yaml.v3"

	"gitlab.com/leducthai/basicgin/db"
	"gitlab.com/leducthai/basicgin/server/configs"
	"gitlab.com/leducthai/basicgin/server/middleware"
	"gitlab.com/leducthai/basicgin/server/request"
)

func parseFlags() *configs.Options {
	var conf configs.Options

	configurations := []swag.CommandLineOptionsGroup{
		{
			ShortDescription: "Server Configuration",
			LongDescription:  "Server Configuration",
			Options:          &conf,
		},
	}

	parser := flags.NewParser(nil, flags.Default)
	for _, optGroup := range configurations {
		if _, err := parser.AddGroup(optGroup.ShortDescription, optGroup.LongDescription, optGroup.Options); err != nil {
			log.Fatalln(err)
		}
	}

	if _, err := parser.Parse(); err != nil {
		code := 1
		if fe, ok := err.(*flags.Error); ok && fe.Type == flags.ErrHelp {
			code = 0
		}
		os.Exit(code)
	}

	return &conf
}

func loadConfig(configPath string) (*configs.Conf, error) {
	var conf configs.Conf

	// read file by path.
	data, err := os.ReadFile(configPath)
	if err != nil {
		return nil, err
	}

	// parse the information to struct.
	if err := yaml.Unmarshal(data, &conf); err != nil {
		return nil, err
	}

	return &conf, nil
}

func Server() {
	server := gin.New()

	// config flags.
	flags := parseFlags()

	// parse config.
	configs, err := loadConfig(flags.Config)
	if err != nil {
		log.Fatalf("load config failed, error: %v", err)
	}

	logger, err := zap.NewDevelopment()
	if err != nil {
		log.Fatalf("failed to initialize logger error: %v", err.Error())
	}
	defer logger.Sync()

	// Use the custom logging middleware
	server.Use(middleware.LoggingMiddleware(logger))

	// register data manager.
	opts := []db.Option{
		db.WithPostgreSQLSchema(configs.PostGres.Schema),
		db.AutoMigrateTables(),
	}

	// new database
	ctx := &gin.Context{}
	datamanager, err := db.New(ctx, db.PGConfig{
		Address:  configs.PostGres.Address,
		Port:     configs.PostGres.Port,
		UserName: configs.PostGres.UserName,
		Password: configs.PostGres.Password,
		Database: configs.PostGres.Name,
	}, opts...)

	defer datamanager.Close()

	server.POST("/users", func(c *gin.Context) {
		var newuser request.PostRequest

		if err := c.BindJSON(&newuser); err != nil {
			c.AbortWithError(http.StatusBadRequest, err)
			return
		}

		reply, err := datamanager.PostUser(c, newuser)
		if err != nil {
			c.AbortWithError(http.StatusBadRequest, err)
			return
		}

		c.IndentedJSON(http.StatusOK, reply)
	})

	address := fmt.Sprintf("%s:%d", flags.Host, flags.Port)

	server.Run(address)
}
