package configs

type Options struct {
	Host   string `long:"host" description:"host that server runs on" default:"localhost" env:"TEST_HOST"`
	Port   int    `long:"port" description:"port that server runs on" default:"8943" env:"TEST_PORT"`
	Config string `long:"config" description:"database config" env:"TEST_CONFIG"`
}

type Conf struct {
	PostGres *PostGresInfo `yaml:"postgres"`
}

type PostGresInfo struct {
	Schema   string `yaml:"schema"`
	Name     string `yaml:"name"`
	Address  string `yaml:"address"`
	Port     int    `yaml:"port"`
	UserName string `yaml:"userName"`
	Password string `yaml:"password"`
}
