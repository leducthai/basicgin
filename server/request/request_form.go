package request

import (
	"encoding/json"

	"github.com/gogo/protobuf/proto"
)

type PostRequest struct {
	Name *string `protobuf:"bytes,1,opt,name=field1" json:"name"`
	Age  *int    `protobuf:"bytes,2,opt,name=field2" json:"age,omitempty"`
	Role *string `protobuf:"bytes,3,opt,name=field3" json:"role"`
}

type RowsAffected = int64

type Reply struct {
	RowsAffecteds RowsAffected
}

func (m *PostRequest) Reset()         { *m = PostRequest{} }
func (m *PostRequest) String() string { return proto.CompactTextString(m) }
func (*PostRequest) ProtoMessage()    {}
func (r *PostRequest) MarshalJSON() ([]byte, error) {
	// Implement custom marshaling logic if necessary
	// You can use json.Marshal or json.MarshalIndent to marshal the struct to JSON
	return json.Marshal(r)
}
