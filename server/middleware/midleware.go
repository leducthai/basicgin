package middleware

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

// CustomResponseWriter is a wrapper around gin.ResponseWriter
type CustomResponseWriter struct {
	gin.ResponseWriter
	Data []byte
}

// Write captures the data written to the response writer
func (w *CustomResponseWriter) Write(data []byte) (int, error) {
	w.Data = append(w.Data, data...)
	return w.ResponseWriter.Write(data)
}

// LoggingMiddleware is a custom middleware that logs all information about the HTTP response using Zap logger
func LoggingMiddleware(logger *zap.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		reqLogFields := []zap.Field{}
		logger.Info("start request", reqLogFields...)
		// Start the timer
		start := time.Now()

		// Create a custom response writer
		customWriter := &CustomResponseWriter{
			ResponseWriter: c.Writer,
			Data:           []byte{},
		}

		// Replace the default response writer with the custom writer
		c.Writer = customWriter

		// Process the request
		c.Next()

		// Calculate the response time
		responseTime := time.Since(start)

		// Log the response information
		statusCode := c.Writer.Status()
		requestMethod := c.Request.Method
		requestPath := c.Request.URL.Path

		responseFields := []zap.Field{
			zap.Int("status", statusCode),
			zap.String("method", requestMethod),
			zap.String("path", requestPath),
			zap.Duration("responseTime", responseTime),
			zap.String("response", string(customWriter.Data)),
		}

		loggingWithLevel(statusCode, responseFields, logger)
	}
}

func loggingWithLevel(statusCode int, responseFields []zap.Field, logger *zap.Logger) {
	switch {
	case statusCode >= http.StatusInternalServerError:
		logger.Error("Response", responseFields...)
	case statusCode >= http.StatusBadRequest:
		logger.Warn("Response", responseFields...)
	default:
		logger.Info("Response", responseFields...)
	}
}
